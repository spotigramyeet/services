#!/usr/bin/bash

echo ""
echo "docker ps"
echo ""

docker ps

echo ""
echo "docker stop spotipi-web"
echo ""

docker stop spotipi-web

echo ""
echo "docker rm spotipi-web"
echo ""

docker rm spotipi-web

echo ""
echo "docker rmi $(docker images --filter "dangling=true" -q --no-trunc)"
echo ""

docker rmi $(docker images --filter "dangling=true" -q --no-trunc)

echo ""
echo "docker build --force-rm -t patrostkowski/spotipi:test ."
echo ""

docker build --force-rm -t patrostkowski/spotipi:test .

echo ""
echo "docker push patrostkowski/spotipi:test"
echo ""

docker push patrostkowski/spotipi:test

echo ""
echo "docker run -d -p 21370:8087 --name spotipi-web patrostkowski/spotipi:test"
echo ""

#docker run -d -p 21370:8087 --name spotipi-web patrostkowski/spotipi:test

echo ""
echo "helm uninstall spotipi-web -n spotipi"
echo ""

helm uninstall spotipi-web -n spotipi

echo ""
echo "helm upgrade -i spotipi-web -n spotipi ../helm/charts/web/"
echo ""

helm upgrade -i spotipi-web -n spotipi ../helm/charts/web/